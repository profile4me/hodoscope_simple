#include "TPRegexp.h"
#include "TObjString.h"
#include "TObjArray.h"
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TreeData.h"

const int EVENTS_LIMIT = 10000;
const char *TXT_DATA_FNAME = "./20201001-144439_HV43V_extTrig_cosmic.txt";
const short GATE_BEG = 25;
const short GATE_END = 45;
const short AMPL_THRES = 100;

FILE *DataFile;
TString buf_string;
TObjArray *buf_tokens;
TObjArray *buf_subtokens;
TPRegexp evLinePat;
Long_t TimeStamp_date;
Long_t TimeStamp_ns;
short **samples_data;
bool readingEventFlag = false;
TFile *outputRootFile = nullptr;
TTree *outputTree = nullptr;
TreeData eventSummaries[NCHANNELS];

enum EventType {
    Noise = 0,
    OneChOverThres = 1,
    OnePairOverThres = 2
};

void MeanRMScalc(short *DataArr, float* Mean, float* RMS, short begin, short end, int step = 1)
{
    begin = (begin < 0)? 0 : begin;

    if(begin > end){float swap=end;end=begin;begin=swap;};
    step = TMath::Abs(step);

    *Mean = *RMS = 0.; int Delta = 0;
    for(int n=begin; n<=end; n += step){ *Mean += DataArr[n]; Delta++;}
    *Mean /= (float)Delta;

    for(int n=begin; n<=end; n += step) *RMS += (DataArr[n] - *Mean) * (DataArr[n] - *Mean);
    *RMS = TMath::Sqrt( *RMS/((float)Delta) );

}

inline TString& getToken(TObjArray *tokens, short n) { return ((TObjString*) tokens->At(n) )->String(); }

void ReadTxtEvent(int ne=0) {

    for (short ch=0; ch<NCHANNELS; ch++)
        for (short si=0; si<NSAMPLES; si++) samples_data[ch][si] = 0;
    while (buf_string.Gets(DataFile)) {
        buf_tokens = buf_string.Tokenize(' ');
        if (!readingEventFlag) {
            if (getToken(buf_tokens, 0).EqualTo("Ev:") && getToken(buf_tokens, 1).Atoi() == ne) {
                readingEventFlag = true;
            }
        }
        else {
            if (!getToken(buf_tokens, 0).EqualTo("Ev:")) {
                short ch = getToken(buf_tokens, 1).Atoi();
                for (short si=0; si<buf_tokens->GetEntries()-3 && si<NSAMPLES; si++) samples_data[ch][si] = getToken(buf_tokens, si+3).Atoi();
            } else {
                fseek(DataFile, ftell(DataFile)-buf_string.Sizeof(), SEEK_SET);
                readingEventFlag = false;
                break;
            }
        }
    }
}

EventType TreeDataCalc(int en, short gateBeg, short gateEnd) {
    Float_t mean1, mean2, rms1, rms2;
    short inMin, inMax, outMin, outMax;
    short amps[NCHANNELS]; 
    EventType res = EventType::Noise;

    for (short ch=0; ch<NCHANNELS; ch++) {
        inMin = outMin = 32767;
        inMax = outMax = -32768;
        MeanRMScalc(samples_data[ch], &mean1, &rms1, 0, gateBeg);
        MeanRMScalc(samples_data[ch], &mean2, &rms2, gateEnd, NSAMPLES);
        eventSummaries[ch].rms_out_gate = (rms1>rms2) ? rms1 : rms2;
        eventSummaries[ch].zero_level = (mean1 + mean2) / 2.0;
        for (short si=0; si<gateBeg; si++) {
            if ( outMin > samples_data[ch][si] ) outMin = samples_data[ch][si];
            if ( outMax < samples_data[ch][si] ) outMax = samples_data[ch][si];
        }
        for (short si=gateEnd; si<NSAMPLES; si++) {
            if ( outMax < samples_data[ch][si] ) outMax = samples_data[ch][si];
            if ( outMin > samples_data[ch][si] ) outMin = samples_data[ch][si];
        }
        eventSummaries[ch].min_out_gate = outMin;
        eventSummaries[ch].max_out_gate = outMax;
        MeanRMScalc(samples_data[ch], &mean1, &rms1, gateBeg, gateEnd);
        eventSummaries[ch].rms_in_gate = rms1;
        for (short si=gateBeg; si<gateEnd; si++) {
            if ( inMin > samples_data[ch][si] ) inMin = samples_data[ch][si];
            if ( inMax < samples_data[ch][si] ) inMax = samples_data[ch][si];
        }
        eventSummaries[ch].min_in_gate = inMin;
        eventSummaries[ch].max_in_gate = inMax;
    
        amps[ch] = eventSummaries[ch].zero_level - eventSummaries[ch].min_in_gate;
        if (amps[ch] > AMPL_THRES) res = EventType::OneChOverThres;
    }

    if (res) for (short ch_pair=0; ch_pair<NCHANNELS/2; ch_pair++) {
        if (amps[ch_pair] > AMPL_THRES && amps[ch_pair+NCHANNELS/2] > AMPL_THRES) return EventType::OnePairOverThres;
    }
    return res;
}

void converter(int eventsLimit = EVENTS_LIMIT, const char* txtFileName = TXT_DATA_FNAME, short gateBeg = GATE_BEG, short gateEnd = GATE_END) {
    
    //init sample buffer, txt data file desciptor and output tree branches
    outputRootFile = new TFile("simpleTree.root", "recreate");
    outputTree = new TTree("adc64_custom", "adc64_custom");
    samples_data = new short*[NCHANNELS];
    for(int ch =0; ch < NCHANNELS; ch++)  {
        samples_data[ch] = new short[NSAMPLES];
        outputTree->Branch(TString::Format("channel_%d", ch).Data(), &eventSummaries[ch], 
            "rms_in_gate/F:rms_out_gate/F:zero_level/F:max_in_gate/S:min_in_gate/S:max_out_gate/S:min_out_gate/S");
    }
    
    DataFile = fopen( txtFileName, "r" );

    //read events
    int decentEvCounters[2] = {0, 0};
    for (int e=0; e<eventsLimit; e++) {
        if (e%1000 == 0) {
            printf("Ev: %d\n", e);
            outputTree->AutoSave();
        }
        ReadTxtEvent(e);
        switch ( TreeDataCalc(e, gateBeg, gateEnd) ) {
            case EventType::OneChOverThres:      decentEvCounters[0]++; break;
            case EventType::OnePairOverThres:    decentEvCounters[1]++; break;
        }
        outputTree->Fill();
    }
    outputTree->Write();
    outputRootFile->Close();
    printf("%d events read. Filtered ones with AMP>AMPL_THRES: %d(in at least ONE CH), %d(in at least ONE PAIR)\n", 
        eventsLimit, decentEvCounters[0], decentEvCounters[1]);
}

//  run as:  ./converter [arg1 [arg2 [arg3 [arg4 ]]]]

//  optional arguments:
//  arg1 - nTot; 
//  arg2 - txtDataFile
//  arg3 - gateBeg
//  arg4 - gateEnd
int main(int argc, char *argv[]) {
    //get command line args
    TString args[10];
    for (int i=1; i<=10; i++) {
        if (i == argc) break;
        args[i-1] = argv[i];
    }    

    switch (argc) {
        case 1: 
            converter(); break; 
        case 2:
            converter(args[0].Atoi()); break;
        case 3:
            converter(args[0].Atoi(), args[1].Data()); break;
        case 4:
            converter(args[0].Atoi(), args[1].Data(), args[2].Atoi()); break;
        case 5:
            converter(args[0].Atoi(), args[1].Data(), args[2].Atoi(), args[3].Atoi()); break;
    }    

    return 0;
}