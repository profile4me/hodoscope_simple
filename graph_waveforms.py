from matplotlib import pyplot, interactive
interactive(True)
import sys


def getSamples(ev, ch):
    f = open("../20201001-144439_HV43V_extTrig_cosmic.txt")
    for line in f:
        tokens = line.split(' ')
        if tokens[0] == 'Ev:' and int(tokens[1]) == ev:
            for ch_i in range(0, 16):
                ch_line = f.readline()
                if ch_i == ch:
                    samples = [ int(x) for x in ch_line.split(' ')[3:] ]
                    break
            break
    return samples

l = input()
while l:
    [ev, ch] = l.split(' ')
    samples = getSamples(int(ev), int(ch))
    print(samples)
    pyplot.plot( list(range(0, 51)), samples )
    pyplot.show()
    l = input()
    pyplot.close()