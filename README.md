Converter of waveforms written by TQDC in TXT format into simple root tree

Compile converter:
```
g++ `root-config --cflags --libs` converter.cpp
```