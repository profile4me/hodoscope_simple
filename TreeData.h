const Short_t NCHANNELS = 16;
const Short_t NSAMPLES = 51;

struct TreeData {
    Float_t rms_in_gate;
    Float_t rms_out_gate;
    Float_t zero_level;

    Short_t max_in_gate;
    Short_t min_in_gate;
    Short_t max_out_gate;
    Short_t min_out_gate;

    TreeData(): rms_in_gate(.0), rms_out_gate(.0), max_in_gate(0), min_in_gate(0), max_out_gate(0), min_out_gate(0) {}
};
